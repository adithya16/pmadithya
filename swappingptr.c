#include <stdio.h>
void swap (int *x,int *y);
int main()
{
    int a,b;
    printf("Enter a & b:");
    scanf("%d %d",&a,&b);
    printf("Numbers before swapping a=%d b=%d",a,b);
    swap(&a,&b);
    printf("\nSwapped numbers a=%d a=%d",a,b);
    return 0;
}
void swap (int *x,int *y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}