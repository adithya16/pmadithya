#include<stdio.h>
#include<math.h>
int input()
{
 int N;
 printf("Enter a number\n");
 scanf("%d",&N);
 return N;
}
float compute(int N)
{
 float sum=0,b;
 int i; 
 for(i=1;i<=N;i++)
 {
  printf("1/%d^2",i);
  if(i!=N)
  printf(" + ");
 }
 for(i=1;i<=N;i++)
 {
  b=(1/pow(i,2));
  sum= sum+b;
 }
 return sum;
}
void output(int N,float sum)
{
 printf("\n");
 printf("Sum of series is: %f\n",sum); 
}
int main()
{
 int N;
 float sum;
 N=input();
 sum=compute(N);
 output(N,sum);
 return 0;
}